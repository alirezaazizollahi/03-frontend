export enum BackendServiceUrl {

    BASE_URL = 'http://localhost:8080/paizan/api',
    GET_PRODUCT_BY_CATEGORY_ID = '/products/findByCategoryId',
    GET_PRODUCT_BY_NAME = '/products/findByName',
    GET_PRODUCT_CATEGORY = '/product-category',
    GET_PRODUCT_BY_ID = '/products/',
    GET_COUNTRY_URL = '/countries',
    GET_STATE_BY_COUNTRY_ID_URL = '/states/findByCountryId',
    POST_PURCHASE_URL = '/checkout/purchase'
}
