import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Purchase } from '../common/purchase';
import { BackendServiceUrl } from '../enums/backend-service-url.enum';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private httpClient: HttpClient) { }

  placeOrder(purchase: Purchase): Observable<any>{
    
    return this.httpClient.post<Purchase>(`${BackendServiceUrl.BASE_URL}${BackendServiceUrl.POST_PURCHASE_URL}`, purchase);
  }
}
