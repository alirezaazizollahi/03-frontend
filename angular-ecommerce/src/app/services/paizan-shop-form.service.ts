import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Country } from '../common/country';
import { State } from '../common/state';
import { BackendServiceUrl } from '../enums/backend-service-url.enum';

@Injectable({
  providedIn: 'root'
})
export class PaizanShopFormService {

  constructor(private httpClient: HttpClient) { }

  getCountries(): Observable<Country[]>{

    return this.httpClient.get<any>
    (`${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_COUNTRY_URL}`).pipe(
      map(response => response)
    );
  }

  getStates(theCountryId: number): Observable<State[]>{
    return this.httpClient.get<any>
    (`${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_STATE_BY_COUNTRY_ID_URL}?id=${theCountryId}`).pipe(
      map(response => response)
    );
  }

  getCreditCardMonth(startMonth: number): Observable<number[]>{

    let data: number[] = [];

    //build an array for Month dropdown list
    // - start at current month and loop until

    for(let theMonth = startMonth; theMonth <= 12; theMonth++){
      data.push(theMonth);
    }

    return of(data);
  }

  getCreditCardYear(): Observable<number[]>{

    let data: number[] = [];

    const startYear: number = new Date().getFullYear();
    const endYear: number = startYear + 10;

    for(let theYear = startYear; theYear <= endYear; theYear++){
      data.push(theYear);
    }

    return of(data);

  }




}
