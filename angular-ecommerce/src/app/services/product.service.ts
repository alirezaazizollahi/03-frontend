import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../common/product';
import { ProductCategory } from '../common/product-category';
import { map } from 'rxjs/operators';
import { BackendServiceUrl } from '../enums/backend-service-url.enum';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
    
  constructor(private httpClient: HttpClient) { }

  getProductListPaginate(thePage: number,
                         thePageSize: number,
                         theCategoryId: number): Observable<GetResponseProducts>{
    // nead to build URL based on category id, page, size  
    const searchUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_BY_CATEGORY_ID}?categoryId=${theCategoryId}`
                     + `&page=${thePage}&size=${thePageSize}`       
    console.log(`searchUrl : ${searchUrl}`);
    console.log(`thePageSizethePageSize : ${thePageSize}`);                     
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  searchProductPaginate(thePage: number,
                        thePageSize: number,
                        theKeyword: string): Observable<GetResponseProducts> {
    // nead to build URL based on keyword, page, size  
    const searchUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_BY_NAME}?name=${theKeyword}`
                    + `&page=${thePage}&size=${thePageSize}`
    console.log(`searchUrl : ${searchUrl}`);
    console.log(`thePageSizethePageSize : ${thePageSize}`);
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProductList(theCategoryId: number): Observable<Product[]>{
    // nead to build URL based on category id  
    const searchUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_BY_CATEGORY_ID}?categoryId=${theCategoryId}`;
    return this.getProducts(searchUrl);
  }

  getProductCategories(): Observable<ProductCategory[]>{ 
    const categoryUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_CATEGORY}`;
    return this.httpClient.get<any>(categoryUrl).pipe(
      map(response => response)
    )
  }

  searchProducts(theKeyword: string): Observable<Product[]> {
    // nead to build URL based on the keyword 
    const searchUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_BY_NAME}?name=${theKeyword}`;
    return this.getProducts(searchUrl);
  }


  private getProducts(searchUrl: string): Observable<Product[]> {
    return this.httpClient.get<any>(searchUrl).pipe(
      map(response => response)
    );
  }

  getProduct(theProductId: number): Observable<Product> {
    const productUrl = `${BackendServiceUrl.BASE_URL}${BackendServiceUrl.GET_PRODUCT_BY_ID}${theProductId}`;
    return this.httpClient.get<any>(productUrl);
  }
 
}

interface GetResponseProducts{
  contents: Product[];
  size: number;
  number: number;
  totalPages: number;
  totalElements: number;
}

