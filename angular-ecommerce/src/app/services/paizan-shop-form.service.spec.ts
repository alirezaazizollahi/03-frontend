import { TestBed } from '@angular/core/testing';

import { PaizanShopFormService } from './paizan-shop-form.service';

describe('PaizanShopFormService', () => {
  let service: PaizanShopFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaizanShopFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
