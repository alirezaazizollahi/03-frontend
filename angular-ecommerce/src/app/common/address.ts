export class Address {

    street: string;
    city: string;
    state: string;
    country: string;
    stateId: number;
    countryId: number;
    zipCode: string;
}
