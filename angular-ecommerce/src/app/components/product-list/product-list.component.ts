import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/common/cart-item';
import { Product } from 'src/app/common/product';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  // templateUrl: './product-list.component.html',
  // templateUrl: './product-list-table.component.html',s
  templateUrl: './product-list-grid.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[] = [];
  currentCategoryId: number = 1;
  previousCategoryId: number = 1;
  searchMode: boolean = false;

  //new properties for pagination
  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;

  previousKeyword: string = null;

  constructor(private productService: ProductService,
              private cartService: CartService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.listProducts();
    });
  }
  listProducts() {
    this.searchMode = this.route.snapshot.paramMap.has('keyword');
    
    if(this.searchMode){
      this.handleSearchProducts();
    }else{
      this.handleListProducts();
    }  
  }

  handleSearchProducts(){
    const theKeyword = this.route.snapshot.paramMap.get('keyword');

    // if we have a different keyword than previous
    // then set pageNumber to 1

    if(this.previousKeyword != theKeyword){
      this.thePageNumber = 1;
    }

    this.previousKeyword = theKeyword;

    console.log(`theKeyword: ${theKeyword}, thePageNumber: ${this.thePageNumber}`);

    // now search for the products using keyword
    this.productService.searchProductPaginate(this.thePageNumber - 1, this.thePageSize ,theKeyword).subscribe(this.processResult());
    
  }

  handleListProducts() {
    // check if id param is avaiable
    const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    if (hasCategoryId) {
      // get id param string. convert string to number using + symbol.
      this.currentCategoryId = +this.route.snapshot.paramMap.get('id');
    } else {
      // not id category available
      this.currentCategoryId = 1;
    }

    //
    // check if we have different category than previous
    // Note: Angular will reuse a component if it is curtently been wiewed
    //

    //
    // if we have a different category than previous
    // then we set the pageNumber to 1
    //

    console.log(`this.previousCategoryId: ${this.previousCategoryId}`);
    console.log(`this.thePageSize: ${this.thePageSize}`);

    if (this.previousCategoryId != this.currentCategoryId){
      this.thePageNumber = 1;
    }

    this.previousCategoryId = this.currentCategoryId;
    console.log(`currentCategoryId : ${this.currentCategoryId}, thePageNumber: ${this.thePageNumber}`);

    /*this.productService.getProductList(this.currentCategoryId).subscribe(
      data => {
        console.log(data);
        this.products = data;
      }
    )*/

    this.productService.getProductListPaginate(this.thePageNumber - 1,
                                               this.thePageSize,
                                               this.currentCategoryId).subscribe(this.processResult());


    

  }

  processResult(){
    return data => {
      this.products = data.content;
      this.thePageSize = data.size ;
      this.thePageNumber = data.number + 1;
      this.theTotalElements = data.totalElements;
    }

  }

  updatePageSize(pageSize: number){
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listProducts();
  }

  addToCart(theProduct: Product){

    console.log(`add to cart: ${theProduct.name}, ${theProduct.unitPrice}`);

    const theCartItem = new CartItem(theProduct);
    this.cartService.addToCart(theCartItem);
  }


}
