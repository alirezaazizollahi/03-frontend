import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, RouterPreloader } from '@angular/router';
import { Country } from 'src/app/common/country';
import { State } from 'src/app/common/state';
import { CartService } from 'src/app/services/cart.service';
import { PaizanShopFormService } from 'src/app/services/paizan-shop-form.service';
import { PaizanShopValidators } from 'src/app/validators/paizan-shop-validators';
import { CheckoutService } from 'src/app/services/Checkout.service';
import { Order } from 'src/app/common/order';
import { OrderItem } from 'src/app/common/order-item';
import { Purchase } from 'src/app/common/purchase';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkoutFormGroup: FormGroup;

  totalPrice: number = 0;
  totalQuantity: number = 0;

  creditCardYears: number[] = [];
  creditCardMonths: number[] = [];

  countries: Country[] = [];
  shippingAddressStates: State[] = [];
  billingAddressStates: State[] = [];

  constructor(private formBuilder: FormBuilder,
              private cartService: CartService,
              private paizanShopFormService: PaizanShopFormService,
              private checkoutService: CheckoutService,
              private rooter: Router) { }

  ngOnInit(): void {
    
    this.defineCheckoutFormGroup();

    this.reviewCardDetails();
    
    // populdate credit card months
    this.populateCreditCardMonths();

    // populdate credit card years
    this.populateCreditCardYears();

    //populate countries
    this.getCountries();

  }

  defineCheckoutFormGroup() {

    this.checkoutFormGroup = this.formBuilder.group({
      customer: this.formBuilder.group({
        firstName: new FormControl('',
         [Validators.required,
          Validators.minLength(2),
          PaizanShopValidators.notOnlyWhitespace]),

        lastName: new FormControl('',
         [Validators.required,
          Validators.minLength(2),
          PaizanShopValidators.notOnlyWhitespace]),

        email: new FormControl('',
            [Validators.required,
             Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
             PaizanShopValidators.notOnlyWhitespace])
      }),
      shippingAddress: this.formBuilder.group({
        street: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace]),

        city: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace]),

        state: new FormControl('',[Validators.required]),
        country: new FormControl('',[Validators.required]),

        zipCode: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace])
      }),
      billingAddress: this.formBuilder.group({
        street: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace]),

        city: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace]),

        state: new FormControl('',[Validators.required]),
        country: new FormControl('',[Validators.required]),

        zipCode: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace])
      }),
      creditCard: this.formBuilder.group({
        cardType: new FormControl('',[Validators.required]),

        nameOnCard: new FormControl('',
        [Validators.required,
         Validators.minLength(2),
         PaizanShopValidators.notOnlyWhitespace]),

        cardNumber: new FormControl('',[Validators.required, Validators.pattern('[0-9]{16}')]),
        securityCode: new FormControl('',[Validators.required, Validators.pattern('[0-9]{3}')]),
        expirationMonth: [''],
        expirationYear: ['']
      })
    });
  }
  reviewCardDetails() {

    //subscribe to cartService.totalPrice
    this.cartService.totalPrice.subscribe(
      totalPrice => this.totalPrice = totalPrice
    );

    //subscribe to cartService.totalQuantity
    this.cartService.totalQuantity.subscribe(
      totalQuantity => this.totalQuantity = totalQuantity
    );
  }

  get firstName(){ return this.checkoutFormGroup.get('customer.firstName'); }
  get lastName(){ return this.checkoutFormGroup.get('customer.lastName'); }
  get email(){ return this.checkoutFormGroup.get('customer.email'); }

  get shippingAddressStreet(){ return this.checkoutFormGroup.get('shippingAddress.street'); }
  get shippingAddressCity(){ return this.checkoutFormGroup.get('shippingAddress.city'); }
  get shippingAddressState(){ return this.checkoutFormGroup.get('shippingAddress.state'); }
  get shippingAddressCountry(){ return this.checkoutFormGroup.get('shippingAddress.country'); }
  get shippingAddressZipCode(){ return this.checkoutFormGroup.get('shippingAddress.zipCode'); }

  get billingAddressStreet(){ return this.checkoutFormGroup.get('billingAddress.street'); }
  get billingAddressCity(){ return this.checkoutFormGroup.get('billingAddress.city'); }
  get billingAddressState(){ return this.checkoutFormGroup.get('billingAddress.state'); }
  get billingAddressCountry(){ return this.checkoutFormGroup.get('billingAddress.country'); }
  get billingAddressZipCode(){ return this.checkoutFormGroup.get('billingAddress.zipCode'); }

  get creditCardType(){ return this.checkoutFormGroup.get('creditCard.cardType'); } 
  get creditCardNameOnCard(){ return this.checkoutFormGroup.get('creditCard.nameOnCard'); } 
  get creditCardNumber(){ return this.checkoutFormGroup.get('creditCard.cardNumber'); } 
  get creditCardSecurityCode(){ return this.checkoutFormGroup.get('creditCard.securityCode'); } 

  copyShippingAddressToBillingAddress(event) {

    if (event.target.checked) {
      this.checkoutFormGroup.controls.billingAddress
            .setValue(this.checkoutFormGroup.controls.shippingAddress.value);

      this.billingAddressStates = this.shippingAddressStates;      
    }
    else {
      this.checkoutFormGroup.controls.billingAddress.reset();

      this.billingAddressStates = [];
    }
    
  }

  populateCreditCardMonths() {
    const startMonth: number = new Date().getMonth() + 1;
    console.log(`startMonth: ${startMonth}`);
    this.paizanShopFormService.getCreditCardMonth(startMonth).subscribe(
      
      data => {
        console.log("retrieved credit card months: " + JSON.stringify(data));
        this.creditCardMonths = data
      }  
    )
  }

  populateCreditCardYears() {
    this.paizanShopFormService.getCreditCardYear().subscribe(
      data => {
        console.log("retrieved credit card years: " + JSON.stringify(data));
        this.creditCardYears = data
      }  
    )
  }

  handlMonthAndYear() {

    const creditCardFormGroup = this.checkoutFormGroup.get('creditCard');
    const currentYear: number = new Date().getFullYear();
    const selectedYear: number = Number(creditCardFormGroup.value.expirationYear);

    // if the current year equals to the selected year, then start with the current month

    let startMonth: number;

    if(currentYear === selectedYear){
      startMonth = new Date().getMonth() + 1;
    }else{
      startMonth = 1;
    }

    this.paizanShopFormService.getCreditCardMonth(startMonth).subscribe(
      data => this.creditCardMonths = data
    );

  }

  getCountries() {

    this.paizanShopFormService.getCountries().subscribe(
      data => {
        console.log(`Retrieved countries: ${JSON.stringify(data)}`);
        this.countries = data;
      }  
    );
  }

  getStates(formGroupName: string) {
    
    const formGroup = this.checkoutFormGroup.get(formGroupName);
    const countryId = formGroup.value.country.id;
    const countryName = formGroup.value.country.name;
    const countryCode = formGroup.value.country.code;
    console.log(`country id: ${countryId}, country name: ${countryName}, country code: ${countryCode}`);
    this.paizanShopFormService.getStates(countryId).subscribe(
      data => {
        console.log(`Retrieved states: ${data}`);
        if (formGroupName === 'shippingAddress'){
          this.shippingAddressStates = data;
        }else if(formGroupName === 'billingAddress'){
          this.billingAddressStates = data;
        }

        // select first item by default
        formGroup.get("state").setValue(data[0]);
        
      }  
    );
  }

  onSubmit() {

    // console.log("Handling the submit button");
    // console.log(this.checkoutFormGroup.get('customer').value);
    // console.log("The email address is " + this.checkoutFormGroup.get('customer').value.email);

    // console.log("The shipping address country is " + this.checkoutFormGroup.get('shippingAddress').value.country.name);
    // console.log("The shipping address state name is " + this.checkoutFormGroup.get('shippingAddress').value.state.name);
    // console.log("The shipping address state country name is " + this.checkoutFormGroup.get('shippingAddress').value.state.country.name);

    console.log('foooorm' + this.checkoutFormGroup.invalid);
    if (this.checkoutFormGroup.invalid){
      this.checkoutFormGroup.markAllAsTouched();
      return;
    }

    // set up order
    let order = new Order();
    order.totalPrice = this.totalPrice;
    order.totalQuantity = this.totalQuantity;

    // get cart items
    const cartItems = this.cartService.cartItems;
    
    // get orderItems from cartItems
    let orderItems = cartItems.map(tempCartItem => new OrderItem(tempCartItem));

    // set up purchase
    let purchase: Purchase = new Purchase();

    // populate purchase - customer
    purchase.customer = this.checkoutFormGroup.controls['customer'].value;

    // populate purchase - shipping address
    purchase.shippingAddress = this.checkoutFormGroup.controls['shippingAddress'].value;
    const shippingState: State = JSON.parse(JSON.stringify(purchase.shippingAddress.state));
    const shippingCountry: Country = JSON.parse(JSON.stringify(purchase.shippingAddress.country));
    purchase.shippingAddress.state = shippingState.name;
    purchase.shippingAddress.stateId = shippingState.id;
    purchase.shippingAddress.country = shippingCountry.name;
    purchase.shippingAddress.countryId = shippingCountry.id;

    // populate purchase - billing address
    purchase.billingAddress = this.checkoutFormGroup.controls['billingAddress'].value;
    const billingState: State = JSON.parse(JSON.stringify(purchase.billingAddress.state));
    const billingCountry: Country = JSON.parse(JSON.stringify(purchase.billingAddress.country));
    purchase.billingAddress.state = billingState.name;
    purchase.billingAddress.stateId = billingState.id;
    purchase.billingAddress.country = billingCountry.name;
    purchase.billingAddress.countryId = billingCountry.id;

    // populate purchase - order and orderItems
    purchase.order = order;
    purchase.orderItems = orderItems;

    // call REST API via CheckoutService
    this.checkoutService.placeOrder(purchase).subscribe({
      next: response => {
        alert(`Your order has been recieved.\n Order tracking number: ${response.orderTrackingNumber}`);

        // reset cart
        this.resetCart();
      },
      error: err => {
        alert(`there was an error: ${err.message}`);
      }
    })

  }
  resetCart() {
    // reset the cart data
    this.cartService.cartItems = [];
    this.cartService.totalPrice.next(0);
    this.cartService.totalQuantity.next(0);

    // reset the form
    this.checkoutFormGroup.reset();

    // navigate back to the product page
    this.rooter.navigateByUrl("/products");

  }

}
