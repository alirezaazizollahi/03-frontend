import { FormControl, ValidationErrors } from "@angular/forms";

export class PaizanShopValidators {

    // whitespace validation

    static notOnlyWhitespace(control: FormControl): ValidationErrors{

        // check if string only contains witespace
        if(control.value != null && control.value.trim().length ===0){

            // invalid, return error object
            return {'notOnlyWhitespace' : true};
        }else{
            return null;
        }
    }
}
